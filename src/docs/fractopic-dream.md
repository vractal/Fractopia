        
### A Fractopic dream

We'll tell a story, maybe a future one, about Fractopia, and how it could be used. We hope that it gives a better idea of what we can build with Fractopia
ane lives in Manaus, 37, and wanted to learn how to grow her own food. She doesn't know how, but she hears there is a place she can find communities about pretty much anything people do. She decides to try it out. "Fractopia, weird name". Seemed a little confusing at first, but after a while it started to make sense.

She had a space to keep things, and connect things together. She had Portals through which she could see and interact with things. After that, she decided to venture in the social portal. There, she found a big community from Manaus, and through its portals got to know a lot of different parts of the town.

Among them, a group of permacultors, whom happily replied in joining and added her in their space. saw a portal with up to date information like best vegetables to grown that month, projections, curated news (actually, 4 different collections sometimes quite disagreeing), weather reports. In another, people discussed their experiences and learned together in permaculture and non-permaculture related topics. They had a so called collection with information about every plant and animal you could imagine. Some were from their own space, some were shared and linked from all over the globe.

Another was created by a solidary econommy cooperative and made easy to keep track and organize the local economy. Has been replicating in the whole world..

And none of this people are developers, or even tech oriented. They just had an way to manipulate data and relations in a human way.. like moving boxes around.
